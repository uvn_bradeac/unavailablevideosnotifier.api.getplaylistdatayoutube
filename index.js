const axios = require('axios')

exports.handler = (event, context, callback) => {
    const channelId = event.query.channelid

    const { base, properties, apiKey } = process.env
    const baseUrl = `${base}${channelId}${properties}${apiKey}`
    
    let playlists = []

    let getPlaylistsData = async(url, pageToken) => {
        if(pageToken) {
            url += `&pageToken=${pageToken}`
        }
    
        axios.get(url)
            .then(response => {
                if (response.error) {
                    callback(response.error.errors.message, null)
                }
                
                const nextPageToken = response.data.nextPageToken

                response.data.items.map(item => {
                    const data = {
                        id: item.id,
                        data: item.snippet,
                        count: item.contentDetails.itemCount,
                    }

                    playlists = [data, ...playlists]
                })
    
                if(nextPageToken) {
                    getPlaylistsData(baseUrl, nextPageToken)
                }
                else {
                    const sorted = playlists.sort((a, b) => {
                        const titleA = a.data.title.toUpperCase()
                        const titleB = b.data.title.toUpperCase()
                        
                        if (titleA < titleB) {
                            return -1
                        }
                        if (titleA > titleB) {
                            return 1
                        }
                
                        return 0
                    })
                    
                    callback(null, playlists)
                }
            })
            .catch(error => callback(error.response.data.error.message, null))
    }
    
    getPlaylistsData(baseUrl, '')
}